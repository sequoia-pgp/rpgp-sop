//! An implementation of the Stateless OpenPGP Command Line Interface
//! using rpgp.
//!
//! This implements a subset of the [Stateless OpenPGP Command Line
//! Interface] using the GPGME OpenPGP implementation.
//!
//!   [Stateless OpenPGP Command Line Interface]: https://datatracker.ietf.org/doc/draft-dkg-openpgp-stateless-cli/

use std::{
    io::{stdin, stdout, Cursor, Read, Write},
    path::{Path, PathBuf},
};

use anyhow::Context as _;
use structopt::StructOpt;

use pgp::{
    armor,
    composed::{
        Deserializable,
        Message,
        KeyType, KeyDetails, SecretKey, SecretSubkey,
        SignedSecretKey,
        key::{
            SecretKeyParamsBuilder,
            SubkeyParamsBuilder,
        },
    },
    packet::{
        Packet,
        LiteralData,
        KeyFlags,
        UserAttribute,
        UserId,
    },
    types::{PublicKeyTrait, SecretKeyTrait, CompressionAlgorithm},
    crypto::{sym::SymmetricKeyAlgorithm, hash::HashAlgorithm},
    ser::Serialize,
};

mod errors;
use errors::{Error, print_error_chain};
type Result<T> = anyhow::Result<T>;

mod cli;
use cli::{
    SOP, SignAs, EncryptAs,
    load_file, create_file, load_certs, load_keys, frob_passwords,
};
mod dates;

fn main() {
    use std::process::exit;

    match real_main() {
        Ok(()) => (),
        Err(e) => {
            print_error_chain(&e);
            if let Ok(e) = e.downcast::<Error>() {
                exit(e.into())
            }
            exit(1);
        },
    }
}

fn real_main() -> Result<()> {
    match SOP::from_args() {
        SOP::Version {} => {
            println!("rpgp-sop {}", "xxx");
        },

        SOP::GenerateKey { no_armor, mut userids, } => {
            if userids.len() == 0 {
                return Err(anyhow::anyhow!(
                    "Generating UID-less keys not supported"));
            }

            let primary_uid = userids.remove(0);
            let mut key_params = SecretKeyParamsBuilder::default();
            key_params
                .key_type(KeyType::Rsa(2048))
                .can_create_certificates(false)
                .can_sign(true)
                .primary_user_id(primary_uid)
                .user_ids(userids)
                .subkeys(vec![
                    SubkeyParamsBuilder::default()
                        .key_type(KeyType::Rsa(2048))
                        .can_encrypt(true)
                        .build().map_err(|e| {
                            anyhow::anyhow!("Failed to generate key: {}", e)
                        })?,
                ])
                //.preferred_symmetric_algorithms(smallvec![
                //    SymmetricKeyAlgorithm::AES256,
                //])
                //.preferred_hash_algorithms(smallvec![
                //    HashAlgorithm::SHA2_256,
                //])
                //.preferred_compression_algorithms(smallvec![
                //    CompressionAlgorithm::ZLIB,
                //])
                ;
            let secret_key_params = key_params.build()
                .map_err(|e| anyhow::anyhow!("Failed to generate key: {}", e))?;
            let secret_key = secret_key_params.generate()?;
            let passwd_fn = || String::new();
            let signed_secret_key = secret_key.sign(passwd_fn)?;
            if no_armor {
                signed_secret_key.to_writer(&mut stdout())?;
            } else {
                signed_secret_key.to_armored_writer(&mut stdout(), None)?;
            }
        },

        SOP::ExtractCert { no_armor, } => {
            let mut buf = Vec::new();
            stdin().read_to_end(&mut buf)?;
            let signed_secret_key =
                SignedSecretKey::from_armor_single(Cursor::new(&buf))
                .map(|(key, _)| key)
                .or_else(|_| SignedSecretKey::from_bytes(&*buf))?;

            let public_key = signed_secret_key.public_key();
            let signed_public_key = public_key.sign(&signed_secret_key,
                                                    || "".into())?;
            if no_armor {
                signed_public_key.to_writer(&mut stdout())?;
            } else {
                signed_public_key.to_armored_writer(&mut stdout(), None)?;
            }
        },

        SOP::Sign { no_armor, as_, keys, } => {
            // XXX: Creating detached signatures seems not to be
            // supported, we can work around that by creating a signed
            // message and detaching the signature.
            match keys.len() {
                0 =>
                    return Err(anyhow::anyhow!("No signing key given")),
                1 => (),
                _ =>
                    return Err(anyhow::anyhow!(
                        "Generating more than one signature is not supported")),
            };
            let key = load_keys(&keys)?.remove(0);

            let mut buf = Vec::new();
            stdin().read_to_end(&mut buf)?;
            let data = if let SignAs::Text = as_ {
                LiteralData::from_str("", &String::from_utf8(buf)?)
            } else {
                LiteralData::from_bytes("", &buf)
            };
            let message = Message::Literal(data);
            // XXX: This will use the primary key.  If that happens to
            // be signing-capable, we are in luck.  There is no way to
            // select the appropriate subkey for the signing
            // operation.
            let message = message.sign(&key, || "".into(), Default::default())?;
            let sig = if let Message::Signed { signature, .. } = message {
                signature
            } else {
                unreachable!()
            };

            // Add packet framing.
            let sig = Packet::from(sig);

            if no_armor {
                sig.to_writer(&mut stdout())?;
            } else {
                armor::write(&sig, armor::BlockType::Signature, &mut stdout(),
                             None)?;
            }
        },

        SOP::Verify { not_before, not_after, signatures, certs, } => {
            unimplemented!()
        },

        SOP::Encrypt { no_armor, as_, with_password, sign_with, certs, } =>
        {
            unimplemented!()
        },

        SOP::Decrypt {
            session_key_out,
            with_session_key,
            with_password,
            verify_out,
            verify_with,
            verify_not_before,
            verify_not_after,
            key,
        } => {
            unimplemented!()
        },

        SOP::Unsupported(args) => {
            return Err(anyhow::Error::from(Error::UnsupportedSubcommand))
                .context(format!("Subcommand {} is not supported", args[0]));
        },

        _ => {
            return Err(anyhow::Error::from(Error::UnsupportedSubcommand))
                .context(format!("Subcommand is not supported"));
        },
    }
    Ok(())
}
